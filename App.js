/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React from 'react';
import {View} from 'react-native';
import {generateSudoku, editValue, markAsFilled} from './src/utils/handlers';
import {
  solvingBackgroundColor as backgroundColor,
  EASY,
} from './src/utils/constants';
import {compose, withState, withHandlers} from 'recompose';
import SudokuBoard from './src/components/SudokuBoard';
import Control from './src/components/Control';
import Keypad from './src/components/Keypad';
const App = ({
  keypadClickHandler,
  elements,
  setElements,
  selectedItem,
  setSelectedItem,
  level,
  setLevel,
  handleSolveAll,
}) => {
  return (
    <View
      style={{
        flex: 1,
        padding: 10,
        alignItems: 'center',
        justifyContent: 'space-between',
      }}>
      <SudokuBoard
        elements={elements}
        selectedItem={selectedItem}
        setSelectedItem={setSelectedItem}
      />

      <Control
        handleSolveAll={handleSolveAll}
        setElements={setElements}
        generateSudoku={generateSudoku}
        level={level}
        setLevel={setLevel}
      />

      <Keypad keypadClickHandler={keypadClickHandler} />
    </View>
  );
};

export default compose(
  withState('elements', 'setElements', generateSudoku(35)),
  withState('selectedItem', 'setSelectedItem', null),
  withState('level', 'setLevel', Number(EASY)),
  withHandlers({
    itemSelectHandler: ({selectedItem, setSelectedItem}) => index =>
      setSelectedItem(index === selectedItem ? null : index),
    keypadClickHandler: ({selectedItem, elements, setElements}) => v => {
      if (selectedItem || selectedItem === 0) {
        const newElements = editValue({
          elements,
          item: 'value',
          value: v,
          index: Number(selectedItem),
        });

        const checkAndMark = markAsFilled({
          elements: newElements,
          index: Number(selectedItem),
        });
        setElements(checkAndMark);
      }
    },
    handleSolveAll: ({elements, setElements}) => () => {
      const solving = elements.map(s =>
        s.map(v => ({
          ...v,
          value: v.correctValue,
          backgroundColor,
        })),
      );

      setElements(solving);
    },
  }),
)(App);
