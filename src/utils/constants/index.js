import range from './range';

const sqr = 3; // original number

const row = sqr ** 2; // length of row
const all = new Array(row * row); // all elements
const entryRange = range(row).map((_, i) => i + 1);

const numOfSections = row / sqr;
const sectionCapacity = sqr ** sqr;
const sectionStarts = range(numOfSections).map((v, i) =>
  i === 0 ? 0 : sectionCapacity * i,
);
const solvingBackgroundColor = 'rgba(13, 79, 105, 0.9)';
const solvedSelected = 'rgba(13, 79, 105, 0.1)';

const EASY = '35';
const MEDUIM = '31';
const HARD = '27';
export {
  range,
  EASY,
  MEDUIM,
  HARD,
  sqr,
  row,
  all,
  entryRange,
  numOfSections,
  sectionCapacity,
  sectionStarts,
  solvingBackgroundColor,
  solvedSelected,
};
