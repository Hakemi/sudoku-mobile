import checkEntry from './checkEntry';

export default function editValue({elements, item, value, index}) {
  return elements.map(s =>
    s.map(v => {
      if (v.index === index) {
        const edited = {...v, [item]: value};
        if (item === 'value') {
          const checking = checkEntry({elements, ...v, value});
          edited.color = !checking ? 'red' : 'black';
        }
        return edited;
      }
      return v;
    }),
  );
}
