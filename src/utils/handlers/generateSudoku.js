import {compact, flatten} from 'lodash';
import {
  range,
  numOfSections,
  sqr,
  sectionStarts,
  row,
  entryRange,
  all,
} from '../constants';

// create Arrays of boxes
const createBoxesFromSections = sectionStarts.reduce((accum, startSection) => {
  // number of boxes
  const numberOfBoxes = range(numOfSections);
  // first index of each boxes
  const boxStart = numberOfBoxes.reduce(
    (acc, q, firstIndex) => [
      ...acc,
      firstIndex === startSection
        ? startSection
        : startSection + sqr * firstIndex,
    ],
    [],
  );
  // create boxes base on the first index
  const createBoxesArray = boxStart.reduce((acc, initBox) => {
    // create first row
    const firstRow = range(sqr).map((b, c) =>
      c === 0 ? initBox : initBox + c,
    );
    // duplicate the first row to get the indexes
    const createBox = firstRow.reduce((ac, r) => {
      const createMod = range(firstRow.length).map((_, c) =>
        c === 0 ? r : r + row * c,
      );
      return [...ac, ...createMod];
    }, []);
    return [...acc, createBox];
  }, []);

  return [...accum, ...createBoxesArray];
}, []);

const allValues = Array.apply(null, all).map((v, i) => i);
const cross = new Array(row);

const mod = Array.apply(null, cross).reduce(
  (acc, v, i) => {
    const {ar, current} = acc;
    const isFirst = i === 0;
    return {
      ...acc,
      ar: [...ar, !isFirst ? current + row : row],
      current: !isFirst ? current + row : row,
    };
  },
  {ar: [], current: 0},
);

const dupLicates = mod.ar;

const addingRows = dupLicates.reduce(
  (acc, v, i) => {
    const {ar, smaller, bigger} = acc;

    const insertArray = allValues.filter((v, indo) => {
      if (indo === 0 && bigger === 0) {
        return 1;
      }
      if (indo < smaller && indo >= bigger) {
        return v;
      }
      return null;
    });

    return {
      ...acc,
      ar: [...ar, insertArray],
      bigger: dupLicates[i],
      smaller: dupLicates[i + 1],
    };
  },
  {ar: [], bigger: 0, smaller: dupLicates[0]},
);
const allRows = addingRows.ar;

function findBox(value) {
  return createBoxesFromSections.reduce((acc, v, i) => {
    if (v.includes(value)) {
      return i;
    }
    return acc;
  }, 0);
}

const rowElementsProps = allRows.map((v, i) =>
  v.map(e => ({
    index: e,
    box: findBox(e),
    row: i,
    value: null,
    column: i === 0 ? e + i : e - row * i,
  })),
);

function shiftRight(ar, n) {
  const startingIndex = ar[n];
  const arrLength = ar.length;
  const duplicate = [...ar, ...ar, ...ar];
  const findFirst = compact(duplicate.map((v, i) => v === startingIndex && i));
  const starting = findFirst[0];
  return compact(
    duplicate.map((v, i) => i >= starting && i < starting + arrLength && v),
  );
}

export default function generateSudoku(level) {
  const randomArray = entryRange.sort(() => Math.random() - Math.random());

  const pr = rowElementsProps.reduce(
    (acc, rowValue, rowIndex) => {
      const {previousRow, ar} = acc;
      let shifftedArray;
      if (rowIndex === 0) {
        shifftedArray = previousRow;
      }
      if (Number.isInteger(rowIndex / sqr)) {
        shifftedArray = shiftRight(previousRow, 1);
      } else {
        shifftedArray = shiftRight(previousRow, sqr);
      }

      const enterValues = rowValue.map((v, i) => ({
        ...v,
        correctValue: shifftedArray[i],
      }));
      return {
        ...acc,
        ar: [...ar, enterValues],
        previousRow: enterValues.map(v => v.correctValue),
      };
    },
    {ar: [], previousRow: randomArray},
  );

  const mixed = allValues.sort(() => Math.random() - Math.random());
  const shownValues = mixed.filter((v, i) => {
    if (i < level || v === 0) {
      return v;
    }
  });

  return pr.ar.map(v =>
    v.map(x => {
      const show = shownValues.includes(x.index);
      return {
        ...x,
        show,
        value: show && x.correctValue,
      };
    }),
  );
}
