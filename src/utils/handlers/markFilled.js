import {range, uniq, flatten} from 'lodash';
import {solvingBackgroundColor as backgroundColor, row} from '../constants';

function getValue({item, value, elements}) {
  const filtering = flatten(elements).filter(
    v => v[item] === value && v.value && v.color !== 'red' && v,
  );
  return filtering;
}

function checkAll({elements, item}) {
  return flatten(
    range(row)
      .map(v => {
        const getColoredBox = getValue({item, value: v, elements});
        if (getColoredBox.length === 9) {
          return getColoredBox.map(e => e.index);
        }
      })
      .filter(v => (v === 0 || v) && v),
  );
}

export default function markAsFilled({elements}) {
  const checkAllBoxes = checkAll({elements, item: 'box'});
  const checkAllRows = checkAll({elements, item: 'row'});
  const checkAllColumns = checkAll({elements, item: 'column'});
  const coloredIndex = uniq([
    ...checkAllBoxes,
    ...checkAllColumns,
    ...checkAllRows,
  ]);
  return elements.map(s =>
    s.map(v => {
      if (coloredIndex.includes(v.index)) {
      }
      return {
        ...v,
        backgroundColor: coloredIndex.includes(v.index) && backgroundColor,
      };
    }),
  );
}
