import React from 'react';
import {View, TouchableOpacity, Picker, Text} from 'react-native';
import {EASY, MEDUIM, HARD} from '../../utils/constants';
import styles from './styles';

export default function SudokuBoard({
  handleSolveAll,
  setElements,
  generateSudoku,
  level,
  setLevel,
}) {
  return (
    <View>
      <TouchableOpacity style={styles.buttonStyle} onPress={handleSolveAll}>
        <Text style={{color: 'white'}}>Solve All</Text>
      </TouchableOpacity>
      <TouchableOpacity
        style={styles.buttonStyle}
        onPress={() => setElements(generateSudoku(Number(level)))}>
        <Text style={{color: 'white'}}>New game</Text>
      </TouchableOpacity>

      <View style={styles.pickerContainer}>
        <Picker
          selectedValue={level}
          style={{width: 100}}
          onValueChange={value => {
            const newLevel = Number(value);
            setLevel(value);
            setElements(generateSudoku(newLevel));
          }}>
          <Picker.Item key={EASY} value={EASY} label="Easy" />
          <Picker.Item key={MEDUIM} value={MEDUIM} label="Meduim" />
          <Picker.Item key={HARD} value={HARD} label="Hard" />
        </Picker>
      </View>
    </View>
  );
}
