import {StyleSheet} from 'react-native';
import {solvingBackgroundColor as backgroundColor} from '../../utils/constants';

export default StyleSheet.create({
  buttonStyle: {
    width: 150,
    alignItems: 'center',
    padding: 10,
    backgroundColor,
    borderRadius: 50,
    margin: 3,
  },
  pickerContainer: {
    width: 150,
    alignItems: 'center',
    backgroundColor,
    borderRadius: 50,
    margin: 3,
  },
});
