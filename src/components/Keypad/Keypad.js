import React from 'react';
import {orderBy} from 'lodash';
import {entryRange} from '../../utils/constants';
import styles from './styles';
import {View, TouchableOpacity, Text} from 'react-native';
export default function Keypad({keypadClickHandler}) {
  return (
    <View style={{flexDirection: 'row', margin: 10}}>
      {orderBy(entryRange).map(v => (
        <TouchableOpacity
          key={v}
          style={styles.KeypadStyle}
          onPress={() => keypadClickHandler(v)}>
          <Text>{v}</Text>
        </TouchableOpacity>
      ))}
    </View>
  );
}
