import {StyleSheet} from 'react-native';

export default StyleSheet.create({
  KeypadStyle: {
    height: 35,
    width: 35,
    borderWidth: 1,
    borderColor: 'black',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
