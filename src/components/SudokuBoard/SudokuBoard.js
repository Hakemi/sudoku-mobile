import React from 'react';
import {sqr, solvedSelected} from '../../utils/constants';
import {Text, View, TouchableOpacity} from 'react-native';

function getBackroundColor(index, backgroundColor, selectedItem) {
  if (backgroundColor && selectedItem === index) {
    return solvedSelected;
  }
  if (backgroundColor) {
    return backgroundColor;
  }

  if (selectedItem === index) {
    return 'gray';
  }

  return 'white';
}

export default function SudokuBoard({elements, selectedItem, setSelectedItem}) {
  return (
    <View>
      {elements.map((row, i) => (
        <View key={i} style={{flexDirection: 'row'}}>
          {row.map((element, index) => {
            return (
              <TouchableOpacity
                key={index}
                style={{
                  height: 34,
                  width: 34,
                  borderBottomWidth: Number.isInteger((element.row + 1) / sqr)
                    ? 5
                    : 1,
                  borderRightWidth: Number.isInteger((element.column + 1) / sqr)
                    ? 5
                    : 1,
                  borderLeftWidth: element.column === 0 ? 5 : 1,
                  borderTopWidth: element.row === 0 ? 5 : 1,
                  backgroundColor: getBackroundColor(
                    element.index,
                    element.backgroundColor,
                    selectedItem,
                  ),
                  alignItems: 'center',
                }}
                onPress={() =>
                  !element.show &&
                  setSelectedItem(
                    element.index === selectedItem ? null : element.index,
                  )
                }>
                <Text
                  style={{
                    opacity: element.value ? 1 : 0,
                    color: element.color,
                  }}>
                  {element.value || 0}
                </Text>
              </TouchableOpacity>
            );
          })}
        </View>
      ))}
    </View>
  );
}
