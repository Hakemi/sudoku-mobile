module.exports = {
  root: true,
  extends: '@react-native-community',
  rules: {
    "react/jsx-filename-extension": [0, { "extensions": [".js", ".jsx"] }],
     "react/prop-types": 0,
     "react-native/no-inline-styles":0
  }
};
